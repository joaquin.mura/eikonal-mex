﻿/*  Fast Sweeping Method for Triangular Meshes
	
	Based on "Fast Sweeping Methods for Eikonal Equations on Triangular Meshes"
	Jianliang Qian, Yong-Tao Zhang, and Hong-Kai Zhao
	SIAM J. Numer. Anal., 45(1), 83–107.
	https://doi.org/10.1137/050627083

    Implementation in C-language for MEX-Matlab API

    Joaquin MURA (2019)
*/

#include "mex.h"
#include "stdlib.h"
#include "math.h"


#define P_in   prhs[0]
#define Tri_in prhs[1]
#define Tet_in prhs[2]
#define Dir_in prhs[3]
#define Phi_out plhs[0]

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

#ifndef M_PI
#define M_PI 3.14159265358
#endif // !M_PI


// comment to avoid too many messages
//#define VERBOSE_ 1

/*  Linked list for inversion map */
typedef struct Node {
	int data;
	struct Node* next;
} node;

//node* push(node* head, int value);    // add new data to the list (no *this pointer in C)
void push(node** head, int value);
int size_list(node* head);            // return length of the list
int get_value(node* head, int index); // return the value of head[index]
/*  end of linked list prototypes */

// Sort of doubles using QSort in C
void  sort_double(double *lista, int largolista, int *orden);

// ========== Other prototypes =============
// * Array operations
double norm_L22(double *A, double *B, int size);
double norm_L2(double *A, int size);
double norm_2_(double X, double Y);
double norm_3_(double X, double Y, double Z);
/*
double norm_gradient_tri3(double xA, double yA, double zA, double xB, double yB, double zB, 
	double xC, double yC, double zC, 
	double phiA, double phiB, double phiC, 
	double *area ); */
double dist_point_line(double x0, double y0, double z0, 
	double xA, double yA, double zA, double xB, double yB, double zB);

// * Elementary operations
double local_solver_triangleABC(double a, double b, double c,
	double phiA, double phiB, double phiC, double fc);
bool projectionABC(double x1, double y1, double z1,
	double x2, double y2, double z2,
	double x3, double y3, double z3,
	double x4, double y4, double z4,
	double kx, double ky, double kz);
bool Get_Normal(double A1, double A2, double A3, double E1,
	double B1, double B2, double B3, double E2,
	double *nx1, double *ny1, double *nz1,
	double *nx2, double *ny2, double *nz2);

// * Local solvers
void local_solver2D(double *phi, int *ordre, double *p, const int Np, int *tri, const int NTri,
	int *tet, const int NTetra, int numpointsDirichlet, node **inverse_map, int dim);
void local_solver3D(double *phi, int *ordre, double *p, const int Np, int *tri, const int NTri,
	int *tet, const int NTetra, int numpointsDirichlet, node **inverse_map, int dim);

// * Fast-Sweeping solver
bool fastsweeping(double *p, const int Np, int *tri, const int NTri,
	int *tet, const int NTetra, double *phi, int *dirichlet, const int nd, const int dim);

// ========== end prototypes =================



/* ************************************************* 
				MEXFUNCTION CALL
***************************************************** */

void mexFunction(int nlhs, mxArray *plhs[],  /* Outputs */
  int nrhs, const mxArray *prhs[])  /* Inputs */
{

	/* Check to see if we are on a platform that does not support the compatibility layer. */
	#if defined(_LP64) || defined (_WIN64)
	#ifdef MX_COMPAT_32
	for (i = 0; i<nrhs; i++) {
		if (mxIsSparse(prhs[i])) {
			mexErrMsgIdAndTxt("MATLAB:explore:NoSparseCompat",
				"MEX-files compiled on a 64-bit platform that use sparse array functions "
				"need to be compiled using -largeArrayDims.");
		}
	}
	#endif
	#endif

    /* Note:
    C/MEX   M-code equivalent
    nlhs    nargout
    plhs    varargout
    nrhs    nargin
    prhs    varargin
    */ 

    int *tri, *tet;
	double *p;
    double *phi; // output
    int *dirichlet;
    int Np=0, NTri=0, NTetra=0, Nd=0, dim=0;
	double *trid, *tetd, *dird;

    bool haveTRI = false;
    bool haveTET = false;

    // Check input variables
    if (nrhs != 4)
        mexErrMsgTxt("Must have 4 input arguments: p,tri,tetra,dirichlet (one of tri or tetra arrays can be empty).");

    // Fill "p"
	int numrowP, numcolP;
    if (mxIsComplex(P_in) || !mxIsDouble(P_in) || mxGetNumberOfElements(P_in) < 2)
        mexErrMsgTxt("p must be a double array");
    else {
        numrowP = (int)mxGetM(P_in); // size_t by default
        numcolP = (int)mxGetN(P_in);

		if ((numrowP != 2) && (numrowP != 3)) {
			mexPrintf("size(p) = [%d,%d]\n", numrowP, numcolP);
			mexErrMsgTxt("p must have size(p)=[2 or 3,no. of nodes].");
		}

        Np = MAX(numrowP,numcolP);
        p = mxGetPr(P_in); // mxGetPr = (double *)mxGetData and mxGetData is type (void *)
		// pointer to P_in: p[m + numrowP*n]  ==  A(m+1,n+1), with m,n=0,1, ... numrowP*numcolP-1
    }

	// Fill "tri"
    if (mxGetNumberOfElements(Tri_in)>0) {
        haveTRI = true;
        NTri = (int)( mxGetNumberOfElements(Tri_in)/3.0 );
        if (mxIsComplex(Tri_in) || !mxIsDouble(Tri_in))
            mexErrMsgTxt("tri must be a double array");
        else {
			int numrowTri = (int)mxGetM(Tri_in); // size_t by default
			int numcolTri = (int)mxGetN(Tri_in); // size_t by default
			if (numrowTri != 3) {
				mexPrintf("size(tri) = [%d,%d]\n", numrowTri, numcolTri);
				mexErrMsgTxt("tri must have size(tri)=[3,no. of triangles].");
			}

			trid = mxGetPr(Tri_in); // set as double*
			int nt = (int)mxGetNumberOfElements(Tri_in);

#ifdef VERBOSE_
			mexPrintf("#elem[tri] : %d, ntri=%d\n", nt,NTri);
#endif

			if (nt > 0) {
				tri = (int *)malloc(nt * sizeof(int));
				for (int i = 0; i < nt; i++)
					tri[i] = ((int)trid[i]) - 1; // C/C++ starts from 0, not from 1
			}
			else {
				tri = NULL;
				mexErrMsgTxt("Have Triangles but cannot read array");
			}
        }
    }
  
    // Fill "tetra"
    if (mxGetNumberOfElements(Tet_in)>0) {
        haveTET = true;
        NTetra = (int)( mxGetNumberOfElements(Tet_in)/4.0 );
        if (mxIsComplex(Tet_in) || !mxIsDouble(Tet_in))
            mexErrMsgTxt("tetra must be a double array");
        else {
			int numrowTet = (int)mxGetM(Tet_in); // size_t by default
			int numcolTet = (int)mxGetN(Tet_in); // size_t by default
			if (numrowTet != 4) {
				mexPrintf("size(tetra) = [%d,%d]\n", numrowTet, numcolTet);
				mexErrMsgTxt("tetra must have size(tetra)=[4,no. of tetrahedra].");
			}

			tetd = mxGetPr(Tet_in); // set as double*
			int nt = (int)mxGetNumberOfElements(Tet_in);
#ifdef VERBOSE_
			mexPrintf("#elem[tetra] : %d, ntetra=%d\n", nt,NTetra);
#endif

			if (nt > 0) {
				tet = (int *)malloc(nt * sizeof(int));
				for (int i = 0; i < nt; i++)
					tet[i] = ((int)tetd[i]) - 1; // C/C++ starts from 0, not from 1
			}
			else {
				tet = NULL;
				mexErrMsgTxt("Have Tetraedra but cannot read array");
			}
        }
    }

	// Fill "dirichlet"
    if (mxGetNumberOfElements(Dir_in)==0 || mxIsComplex(Dir_in) ) //|| mxIsDouble(Dir_in))
        mexErrMsgTxt("dirichlet must be an integer array");
	else {
		dird = mxGetPr(Dir_in); // set as double*
		Nd = (int)mxGetNumberOfElements(Dir_in); // by default is of type size_t
#ifdef VERBOSE_
		mexPrintf("#elem[dir] : %d\n", Nd);
#endif

		if (Nd > 0) {
			dirichlet = (int *)malloc(Nd * sizeof(int));

			for (int i = 0; i < Nd; i++)
				dirichlet[i] = ((int)dird[i]) - 1; // C/C++ starts from 0, not from 1.
		}
		else {
			dirichlet = NULL;
			mexErrMsgTxt("must have a dirichlet array");
		}
	}
        
    // Set problem physical dimension
    if (haveTET)
        dim = 3;
    else if (haveTRI)
        dim = 2;
    else
        mexErrMsgTxt("could not find triangles nor tetrahedra");
  
#ifdef VERBOSE_
	mexPrintf("dim : %d\n", dim);
	mexPrintf("var[0]: length=%d\n", Np);
	for (int i = 0; i < Np; i++) {
		mexPrintf("[%d] %g %g ", i, p[0 + dim * i], p[1 + dim * i]);
		if (numrowP == 3)
			mexPrintf("%g ", p[2 + 3 * i]);
		mexPrintf("\n");
	}
	mexEvalString("pause(.001);"); // to dump string.

	mexPrintf("var[1]: length=%d\n", NTri);
	for (int i = 0; i < NTri; i++) {
		mexPrintf("[%d] %d %d %d\n", i, tri[0 + 3 * i], tri[1 + 3 * i], tri[2 + 3 * i]);
	}
	mexEvalString("pause(.001);"); // to dump string.

	mexPrintf("var[2]: length=%d\n", NTetra);
	for (int i = 0; i < NTetra; i++) {
		mexPrintf("[%d] %d %d %d %d\n", i, tet[0 + 4 * i], tet[1 + 4 * i], tet[2 + 4 * i], tet[3 + 4 * i]);
	}
	mexEvalString("pause(.001);"); // to dump string.

	mexPrintf("var[3]: length=%d\n", Nd);
	for (int i = 0; i < Nd; i++) {
		mexPrintf("[%d] %d\n", i, dirichlet[i]);
	}
	mexEvalString("pause(.001);"); // to dump string.
#endif

	
    // set the output
    Phi_out = mxCreateDoubleMatrix(1,Np,mxREAL); // real-valued data
	phi = (double *)mxGetPr(Phi_out); // get pointer to ouput

    // Call the method
    bool converge = fastsweeping(p,Np,tri,NTri,tet,NTetra,phi,dirichlet,Nd,dim);
	
#ifdef VERBOSE_
	for (int i = 0; i<Np; i++) {
		mexPrintf("phi[%d] = %f\n", i, phi[i]);
		mexEvalString("pause(.001);"); // to dump string.
	}
#endif

	// clean exit
	if (NTri>0)
		free(tri);
	if (NTetra>0)
		free(tet);
	if (Nd>0)
		free(dirichlet);

  return;
} 



/* *****************************************************
              FAST-SWEEPING ALGORITHM
******************************************************** */
bool fastsweeping(double *p, const int Np, int *tri, const int NTri,
	int *tet, const int NTetra, double *phi, int *dirichlet, const int nd, const int dim)
{
	// Convergence conditions
	const double tol_rel = 1E-3; // relative tolerance
	const double tol_abs = 1E-4; // absolute tolerance
	const int MAX_iter_fs = 25;   // MAXimum number of iterations
	bool converge = false;        // set initial convergence status

	int num_prefs;
	int p_refs[6]; // set reference nodes
	double *dist = (double *)malloc(Np * sizeof(double)); // only C99+ permits double dist[Np];
	int **ordre = malloc(5 * sizeof(int *)); // only from C99 permits int ordre[5][np];
	for (int i = 0; i < 5; i++)
		ordre[i] = malloc(Np * sizeof(int));

	/* REMEMBER:
	free((void *)dist);
	for(i = 0; i < 5; i++)
	free((void *)ordre[i]);
	free((void *)ordre);
	*/
	double xr, yr, zr, xi, yi, zi; // distance wrt reference nodes

#ifdef VERBOSE_
	mexPrintf("Np     = %d\n", Np);
	mexPrintf("NTri   = %d\n", NTri);
	mexPrintf("NTetra = %d\n", NTetra);
	mexPrintf("Nd     = %d\n", nd);
	mexPrintf("dim    = %d\n", dim);
	mexEvalString("pause(.001);"); // to dump string.
#endif

	// Assign vectors
	double *phi_anc = (double *)malloc(Np * sizeof(double)); // phi_old
	/* IMPORTANT REMARK:
	THIS ARRAY WAS ALREADY INITIALIZATED WITH mxCreateDoubleMatrix(..)
	if we apply this sentence:
	phi = (double *)malloc(Np * sizeof(double)); // phi_actual
	the output will be 0's, because we are changing the pointer.
	*/

	// ===== INVERSE MAP ======================================================
	// Array of linked lists to associate elements containing the i-th node:
	// #elem = inverse_map[i][*]
	// in C++ is simpler: std:vector< std:vector<int> > inverse_map;
	//                    inverse_map.resize(Np); inverse_map[7].push_back(3); ...

	node **inverse_map = (node **)malloc(Np * sizeof(node*));
	for (int i = 0; i < Np; i++) inverse_map[i] = NULL;
	
	mexPrintf("Build inverse_map ...");
	if (dim == 2 || NTetra == 0) {
		for (int i = 0; i<NTri; i++) {
#ifdef VERBOSE_
			mexPrintf("[%d of %d | Np=%d]: ", i, NTri, Np);
			mexEvalString("pause(.001);"); // to dump string.
#endif
			int Tv1 = tri[0 + 3 * i]; // tri(1,i)
			int Tv2 = tri[1 + 3 * i]; // tri(2,i)
			int Tv3 = tri[2 + 3 * i]; // tri(3,i)

#ifdef VERBOSE_
			mexPrintf("v1,v2,v3 = %d, %d, %d\n", Tv1,Tv2,Tv3);
			mexEvalString("pause(.001);"); // to dump string.
#endif
			push(&inverse_map[Tv1], i);
			push(&inverse_map[Tv2], i);
			push(&inverse_map[Tv3], i);
		}
	}
	else {
		for (int i = 0; i<NTetra; i++) {
			int Tv1 = tet[0 + 4 * i]; // tet(1,i)
			int Tv2 = tet[1 + 4 * i]; // tet(2,i)
			int Tv3 = tet[2 + 4 * i]; // tet(3,i)
			int Tv4 = tet[3 + 4 * i]; // tet(4,i)

#ifdef VERBOSE_
			mexPrintf("v1,v2,v3,v4 = %d, %d, %d, %d\n", Tv1, Tv2, Tv3,Tv4);
#endif
			push(&inverse_map[Tv1], i);
			push(&inverse_map[Tv2], i);
			push(&inverse_map[Tv3], i);
			push(&inverse_map[Tv4], i);
		}
	}
	mexPrintf(" done.\n");

#ifdef VERBOSE_
	mexPrintf("#Some values ... \n");
	mexPrintf("node 0 ");
	mexEvalString("pause(.001);"); // to dump string.
	int sz = size_list(inverse_map[0]);
	mexPrintf("[length:%d] is in elements ", sz);
	mexEvalString("pause(.001);"); // to dump string.
	for (int i = 0; i<sz; i++) {
		mexPrintf("%d ", get_value(inverse_map[0], i));
		mexEvalString("pause(.001);"); // to dump string.
	}
	mexPrintf("\n");
	mexPrintf("node 1 ");
	mexEvalString("pause(.001);"); // to dump string.
	sz = size_list(inverse_map[1]);
	mexPrintf("[length:%d] is in elements ", sz);
	mexEvalString("pause(.001);"); // to dump string.
	for (int i = 0; i<sz; i++) {
		mexPrintf("%d ", get_value(inverse_map[1], i));
		mexEvalString("pause(.001);"); // to dump string.
	}
	mexPrintf("\n");
#endif

	// ===== end INVERSE_MAP =============================================

	// WARNING: Cannot have coincidence between Dirichlet nodes and reference nodes.
	// ** Choosing reference nodes **
	num_prefs = (dim == 3 && NTetra>0) ? 5 : 4; // 3D is too expensive for too many nodes

	p_refs[0] = (int)(0.1*(double)(Np)+0.5);

	if (dim == 3) {
		//  3D: 6 ref pts
		p_refs[1] = (int)(0.2*(double)(Np)+0.5);
		p_refs[2] = (int)(0.4*(double)(Np)+0.5);
		p_refs[3] = (int)(0.6*(double)(Np)+0.5);
		p_refs[4] = (int)(0.8*(double)(Np)+0.5);
	}
	else {
		//  2D: 4 ref pts
		p_refs[1] = (int)(0.25*(double)(Np)+0.5);
		p_refs[2] = (int)(0.75*(double)(Np)+0.5);
	}
	p_refs[num_prefs - 1] = (int)(0.9*(double)(Np)+0.5);

#ifdef VERBOSE_
	mexPrintf(" - avoiding superpositions between reference lists.\n");
#endif

	// Avoid coincidence when p_ref[i]==0
	if (p_refs[1] == 10 || p_refs[1] == 2)
		p_refs[1] += 1;

	// check:
	for (int i = 0; i<num_prefs; i++) {
		for (int j = 0; j<nd; j++) {

			int R1 = rand() % 99 + 1; // pseudo-random between 1 and 99
			int R2 = rand() % 99 + 1; // pseudo-random between 1 and 99

			int N1 = (int)((double)(Np)*(double)(R1) / 101.0 + 0.5);
			int N2 = (int)((double)(Np)*(double)(R2) / 101.0 + 0.5);

			if (p_refs[i] == dirichlet[j]) {
				p_refs[i] = N1 + 1; // <----------- TEST????

				if (p_refs[i] == 0) { // Si llega al primer nodo
					p_refs[i] = (int)((double)(N1 + Np) / 2.0);
				}

				if (p_refs[i] >= Np)
					p_refs[i] = N2;


#ifdef VERBOSE_
				mexPrintf("Warning: ref.node %d coincide with a Dirichlet node.\n", dirichlet[j]);
				mexPrintf("   moving reference to %d [N1=%d,N2=%d]\n", p_refs[i], N1, N2);
#else
				mexPrintf("*");
#endif
				i = i - 1; // rewind ...
				j = nd;
			}
		}
	}
	mexPrintf("\n"); // newline


	// DeterMINation of distance array wrt reference nodes
#ifdef VERBOSE_
	mexPrintf("#Distance eval wrt reference nodes\n");
	mexEvalString("pause(.001);"); // to dump string.
#endif

	/* Remark: To access to specific element in double* array using Matlab syntax, take
	<C/C++> p[m + numrowP*n]     <===>    <Matlab> p(m+1,n+1),
	with m,n=0,1, ... numrowP*numcolP-1 */


	for (int j = 0; j<num_prefs; j++) {

		xr = p[0 + dim * j];  // x[j] = p(1,j) in MatLab
		yr = p[1 + dim * j];  // y[j] = p(2,j) in MatLab
		zr = (dim == 3) ? p[2 + 3 * j] : 0.0;

		for (int i = 0; i<Np; i++) {
			xi = p[0 + dim * i];
			yi = p[1 + dim * i];
			zi = (dim == 3) ? p[1 + 3 * i] : 0.0;

			// If node i is in "dirichlet", then dist=0
			for (int k = 0; k<nd; k++) {
				if (i == dirichlet[k]) {
					xi = xr;
					yi = yr;
					zi = zr;
					k = nd;
				}
			}

			// Calculate Euclidian distance between reference node 'ref' and node 'i'
			dist[i] = sqrt((xr - xi)*(xr - xi) + (yr - yi)*(yr - yi) + (zr - zi)*(zr - zi));
		}

		// The matrix 'ordre' saves the indexing wrt reference nodes, including Dirichlet 
		// nodes but excluding reference nodes. Therefore, the right values start from 1+nd
		// instead of 0.
#ifdef VERBOSE_
		mexPrintf("# Sorting list with reference nodes no. %d ...",j);
		mexEvalString("pause(.001);"); // to dump string
#endif
		sort_double(dist, Np, ordre[j]);
#ifdef VERBOSE_
		mexPrintf(" done.\n");
		mexEvalString("pause(.001);"); // to dump string
#endif
	} // end of distance references


	  // INITIALIZATION
	for (int i = 0; i<Np; i++)
		phi[i] = 1E+18; // very high value everywhere

	for (int i = 0; i<nd; i++)
		phi[dirichlet[i]] = 0.0; // = 0 on Dirichlet nodes


	// EIKONAL SOLVER LOOP
#ifdef VERBOSE_
	mexPrintf(" ==== Fast-Sweeping iterations ====\n");
#endif

	for (int iter = 0; iter<MAX_iter_fs; iter++) {

		for (int i = 0; i<Np; i++) //  update
			phi_anc[i] = phi[i];

		// ||u_old||_L²
		double error_old = norm_L2(phi_anc, Np);

		// for reference node i,
		for (int i = 0; i<num_prefs; i++) {
			// All except dirichlet
			if ((dim == 2) || ((dim == 3) && (NTri != 0))) {

				local_solver2D(phi, ordre[i], p, Np, tri, NTri, tet, NTetra, nd, inverse_map, dim);

			}
			else {
#ifdef VERBOSE_
				mexPrintf("[3D ref. %d/%d", (i + 1), num_prefs);
				mexEvalString("pause(.001);"); // to dump string
#endif
				local_solver3D(phi, ordre[i], p, Np, tri, NTri, tet, NTetra, nd, inverse_map, dim);
#ifdef VERBOSE_	
				mexPrintf("]\n");
				mexEvalString("pause(.001);"); // to dump string
#endif
			}
		}

		// ||u_new - u_old||_L²
		double error_diff = norm_L22(phi, phi_anc, Np);
		double tolerance = MIN(error_old*tol_rel, tol_abs); // error_old >0! ;-)

#ifdef VERBOSE_
		mexPrintf("  [iter %d] --> residual: %e, tol: %e\n", iter, error_diff, tolerance);
		mexEvalString("pause(.001);"); // to dump string.
#else
		mexPrintf("res: %e (tol=%e)\n", error_diff, tolerance);
		mexEvalString("pause(.001);"); // to dump string.
#endif

		// check for small errors
		if (error_diff < tolerance) {
			converge = true;
			iter = MAX_iter_fs;
			break;
		}

	} // end for contador...
#ifdef VERBOSE_
	if (converge == false)
		mexPrintf("  ==> Max no. of iterations reached (%d)\n", MAX_iter_fs);
#endif

	// The End: Memory release of local functions
	free(phi_anc); phi_anc = NULL;
	free((void *)dist);
	for (int i = 0; i < 5; i++)
		free((void *)ordre[i]);
	free((void *)ordre);
	for (int i = 0; i < Np; i++)
		free((node *)inverse_map[i]);
	free((node **)inverse_map); // node **inverse_map = (node **)malloc(Np * sizeof(node*));


#ifdef VERBOSE_
	mexPrintf("  <--- Normal end with 'FastSweeping'\n");
#endif

	return converge;
}





/* *******************************************
   Declarations for linked lists
*/

int size_list(node* head) {
	int i = 0;

	if (head != NULL) {
		node **nextForPosition = &head;
		while (*nextForPosition != NULL) {
			nextForPosition = &(*nextForPosition)->next;
			i = i + 1;
		}
	}
	return(i); 
}

void push(node** head, int value) {

	node * currentNode = (node *)malloc(sizeof(node));
	
	currentNode->data = value;
	currentNode->next = *head;
	*head = currentNode;
}

int get_value(node* head, int index) {

	int i = 0;
	if (head != NULL) {

		node* c = head;
		while (c != NULL && i != index) {
			c = c->next;
			i = i + 1;
		}

		return(c->data);
	}
	else {
		mexWarnMsgIdAndTxt("nodeGetValue", "node:get_value(NULL) throw -1");
		return(-1);
	}
}



//   ------ SORT --------------------------------------
int compare_doubles(const void *a, const void *b)
{
	const double *da = (const double *)a;
	const double *db = (const double *)b;

	return (*da > *db) - (*da < *db);
}

void  sort_double(double *lista, int largolista, int *orden)
{
	// internal variables
	int i, j;
	double *lista_aux = (double *)malloc(largolista * sizeof(double));

	// Copy of original array
	for (i = 0; i<largolista; i++) {
		for (j = i; j<largolista; j++) { // If two element matches
			if ((lista[i] == lista[j]) && (i != j)) {
				// to avoid problems with "qsort"
				lista[j] = lista[j] + 0.000001;
			}
		}
		lista_aux[i] = lista[i];
	}

	// Ordering
	qsort(lista, largolista, sizeof(double), compare_doubles);

	// (sub-optimal) Search of new location
	for (i = 0; i<largolista; i++) {
		for (j = 0; j<largolista; j++) {
			if (lista_aux[i] == lista[j]) {
				orden[j] = i;
				break;
			}
		}
	}

	// End
	free(lista_aux);

	return;
}


// ------- FUNCTIONS WITH ARRAYS ------
//   ------ NORM_l2 --------------------------------------
double norm_L22(double *A, double *B, int size)
{
	double resultado = 0.;

	for (int i = 0; i<size; i++)
		resultado += (A[i] - B[i])*(A[i] - B[i]);

	return (sqrt(resultado));
}
//   ------ NORM_l2 --------------------------------------
double norm_L2(double *A, int size)
{
	double resultado = 0.;

	for (int i = 0; i<size; i++)
		resultado += A[i] * A[i];

	return (sqrt(resultado));
}

//   ------ NORM_2 --------------------------------------
double norm_2_(double X, double Y)
{
	return(sqrt(X*X + Y * Y));
}
//   ------ NORM_3 --------------------------------------
double norm_3_(double X, double Y, double Z)
{
	return(sqrt(X*X + Y * Y + Z * Z));
}

//   ------ NORM_GRADIENT TRI3 --------------------------------------
// <<>>>>>	IS THIS FUNCTION REALLY USEFUL???? <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
/*
double norm_gradient_tri3(double xA, double yA, double zA, double xB, double yB, double zB,
	double xC, double yC, double zC, double phiA, double phiB, double phiC, double *area)
{
	// Vab,Vac: vectores que unen los puntos A-B y A-C, respectivamente.
	double VabX = xB - xA, VabY = yB - yA, VabZ = zB - zA;
	double VacX = xC - xA, VacY = yC - yA, VacZ = zC - zA;

	// Componentes del producto cruz
	double ax = VabY * VacZ - VabZ * VacY;
	double ay = VabZ * VacX - VabX * VacZ;
	double az = VabX * VacY - VabY * VacX;
	*area = 0.5*sqrt(ax*ax + ay * ay + az * az); // area del triangulo

	double dphidx = 0, dphidy = 0, gradE = 0; // @@ dphidz=0 no se usa!!

	if ((zA == zB) && (zB == zC)) {
		// Genuinamente 2D
		if (yB != yA) {
			dphidx = ((phiC - phiA)*(yB - yA) - (phiB - phiA)*(yC - yA)) / ((xC - xA)*(yB - yA) - (xB - xA)*(yC - yA));
			dphidy = (phiB - phiA - dphidx * (xB - xA)) / (yB - yA);
		}
		else {
			dphidx = (phiB - phiA) / (xB - xA);
			dphidy = (phiC - phiA - dphidx * (xC - xA)) / (yC - yA);
		}

		gradE = sqrt(dphidx*dphidx + dphidy * dphidy);
	}

	return gradE;
}
*/
// -------------------------------------------------------------------------------------
// distancia entre un punto v0=(x0,y0,z0) y la recta que pasa por vA y vB
// (restringida al triangulo 0AB)
double dist_point_line(double x0, double y0, double z0, double xA, double yA, double zA,
	double xB, double yB, double zB)
{
	double v0Ax = xA - x0, v0Ay = yA - y0, v0Az = zA - z0;
	double v0Bx = xB - x0, v0By = yB - y0, v0Bz = zB - z0;
	double vABx = xB - xA, vABy = yB - yA, vABz = zB - zA;

	// componentes del producto cruz v0A x v0B
	double num_x = v0Ay * v0Bz - v0By * v0Az;
	double num_y = v0Bx * v0Az - v0Ax * v0Bz;
	double num_z = v0Ax * v0By - v0Bx * v0Ay;

	double num = sqrt(num_x*num_x + num_y * num_y + num_z * num_z); // |(v0-vA)x(v0-vB)|
	double den = sqrt(vABx*vABx + vABy * vABy + vABz * vABz); // |vB-vA|

	double dist = num / den;

	// Verifica que la distancia (MINima) pase por el interior del triangulo (causalidad)
	double v0AAB = v0Ax * vABx + v0Ay * vABy + v0Az * vABz; // v0A.vAB
	double v0BAB = v0Bx * vABx + v0By * vABy + v0Bz * vABz; // v0B.vAB

	double A = sqrt(v0Ax*v0Ax + v0Ay * v0Ay + v0Az * v0Az);
	double B = sqrt(v0Bx*v0Bx + v0By * v0By + v0Bz * v0Bz);

	// no se satisface la causalidad si las anteriores tienen el mismo signo,
	// entonces se restringe el MINimo a los largos de los lados del triangulo:
	if (v0AAB*v0BAB>0)
		dist = MIN(A, B);

	return dist;
}



// ============ ELEMENTARY SOLVERS FOR FS =======================

// ---------------------------------------------------------------------------------
// Funcion que decide los valores de phi en los nodos del triangulo (polinomio P1)
// ---------------------------------------------------------------------------------
double local_solver_triangleABC(double a, double b, double c,
	double phiA, double phiB, double phiC, double fc)
{
	double alpha, beta, theta;
	double cos_gamma, sin_gamma;
	double h, H;
	cos_gamma = (a*a + b * b - c * c) / (2 * a*b);
	sin_gamma = sqrt(1. - cos_gamma * cos_gamma);

	// il'y a des ocassions dans lesquels pour erreur de troncature
	// que b*sin_gamma/c est un petite peut majeure que 1...
	alpha = asin(MIN(b*sin_gamma / c, 1.));
	beta = asin(MIN(a*sin_gamma / c, 1.));

	// Solveur locale (sur le triangle avec sommets A, B, C)
	if (fabs(phiA - phiB) <= fabs(c*fc)) {
		theta = asin(MIN(fabs(phiA - phiB) / fabs(c*fc), 1.0));

		if (((MAX(0., alpha - M_PI / 2) <= theta) && (theta <= M_PI / 2 - beta)) || ((alpha - M_PI / 2 <= theta) && (theta <= MIN(0., M_PI / 2 - beta)))) {
			h = a * sin(alpha - theta);
			H = b * sin(beta + theta);

			phiC = MIN(phiC, 0.5*(h*fc + phiB) + 0.5*(H*fc + phiA));
		}
		else
			phiC = MIN(phiC, MIN(phiA + b * fc, phiB + a * fc));
	}
	else
		phiC = MIN(phiC, MIN(phiA + b * fc, phiB + a * fc));

	return(phiC);
}



// ------------------------------------------------------------------------------------------
// Funcion que calcula la projection del punto P4 en la direccion k sobre el plano (P1,P2,P3)
// Devuelve 'true' si el punto esta dentro del plano proyectado, 'false' sino.
// ------------------------------------------------------------------------------------------
bool projectionABC(double x1, double y1, double z1,
	double x2, double y2, double z2,
	double x3, double y3, double z3,
	double x4, double y4, double z4,
	double kx, double ky, double kz)
{
	double X, Y, Z, TT, T1, T2, T3;
	double d1, d2, d3;

	// Coordenadas de la proyeccion sobre el plano ABC
	X = (kz*x4*(x3*y1 + x1 * y2 - x3 * y2 - x1 * y3 + x2 * (-y1 + y3)) + (ky*x4 - kx * y4)*(-(x3*z1) - x1 * z2 + x3 * z2 + x2 * (z1 - z3) + x1 * z3) + kx * (-(x3*y1) - x1 * y2 + x3 * y2 + x2 * (y1 - y3) + x1 * y3)*z4) / (kz*(x3*y1 + x1 * y2 - x3 * y2 - x1 * y3 + x2 * (-y1 + y3)) + ky * (x2*z1 - x3 * z1 - x1 * z2 + x3 * z2 + x1 * z3 - x2 * z3) + kx * (-(y2*z1) + y3 * z1 + y1 * z2 - y3 * z2 - y1 * z3 + y2 * z3));

	Y = (kz*(x3*y1 + x1 * y2 - x3 * y2 - x1 * y3 + x2 * (-y1 + y3))*y4 + (ky*x4 - kx * y4)* (-(y3*z1) - y1 * z2 + y3 * z2 + y2 * (z1 - z3) + y1 * z3) + ky * (-(x3*y1) - x1 * y2 + x3 * y2 + x2 * (y1 - y3) + x1 * y3)*z4) / (kz*(x3*y1 + x1 * y2 - x3 * y2 - x1 * y3 + x2 * (-y1 + y3)) + ky * (x2*z1 - x3 * z1 - x1 * z2 + x3 * z2 + x1 * z3 - x2 * z3) + kx * (-(y2*z1) + y3 * z1 + y1 * z2 - y3 * z2 - y1 * z3 + y2 * z3));

	Z = -(((y3*(-z1 + z2) + y2 * (z1 - z3) + y1 * (-z2 + z3))*(kz*x4 - kx * z4) + (x3*(z1 - z2) + x1 * (z2 - z3) + x2 * (-z1 + z3))*(kz*y4 - ky * z4)) / ((kz*(-(x3*y1) - x1 * y2 + x3 * y2 + x2 * (y1 - y3) + x1 * y3) + ky * (-(x2*z1) + x3 * z1 + x1 * z2 - x3 * z2 - x1 * z3 + x2 * z3) + kx * (y2*z1 - y3 * z1 - y1 * z2 + y3 * z2 + y1 * z3 - y2 * z3))));

	/*
	@_ REMARK: with respect to the previous version of this program, the evaluation of Z
	was factorized by a term kz/kz... giving NaN when kz=0. Now we avoid this.
	*/

	// Calculo de Areas de subtriangulos
	d1 = x1 * Y - x2 * Y - X * y1 + x2 * y1 + X * y2 - x1 * y2;
	d2 = -(x1*Z) + x2 * Z + X * z1 - x2 * z1 - X * z2 + x1 * z2;
	d3 = y1 * Z - y2 * Z - Y * z1 + y2 * z1 + Y * z2 - y1 * z2;
	T1 = 0.5*sqrt(d1*d1 + d2 * d2 + d3 * d3);

	d1 = x1 * Y - x3 * Y - X * y1 + x3 * y1 + X * y3 - x1 * y3;
	d2 = -(x1*Z) + x3 * Z + X * z1 - x3 * z1 - X * z3 + x1 * z3;
	d3 = y1 * Z - y3 * Z - Y * z1 + y3 * z1 + Y * z3 - y1 * z3;
	T2 = 0.5*sqrt(d1*d1 + d2 * d2 + d3 * d3);

	d1 = -(x2*Y) + x3 * Y + X * y2 - x3 * y2 - X * y3 + x2 * y3;
	d2 = x2 * Z - x3 * Z - X * z2 + x3 * z2 + X * z3 - x2 * z3;
	d3 = -(y2*Z) + y3 * Z + Y * z2 - y3 * z2 - Y * z3 + y2 * z3;
	T3 = 0.5*sqrt(d1*d1 + d2 * d2 + d3 * d3);

	// Area de triangulo total
	d1 = -(x2*y1) + x3 * y1 + x1 * y2 - x3 * y2 - x1 * y3 + x2 * y3;
	d2 = x2 * z1 - x3 * z1 - x1 * z2 + x3 * z2 + x1 * z3 - x2 * z3;
	d3 = -(y2*z1) + y3 * z1 + y1 * z2 - y3 * z2 - y1 * z3 + y2 * z3;
	TT = 0.5*sqrt(d1*d1 + d2 * d2 + d3 * d3);

	if (T1 + T2 + T3 == TT)
		return (true); //El punto proyectado cae dentro del triangulo
	else
		return (false); //El punto proyectado cae fuera del triangulo

}


/*------------------------------------------------------------------------------
Evaluacion de los vectores normales (nx,ny,nz)_1,2 soluciones de

(A1,A2,A3).(nx,ny,nz) = E1
(B1,B2,B3).(nx,ny,nz) = E2
nx^2 + ny^2 + nz^2  = 1

Devuelve 'true' si hay solucion.

Nota: hay dos soluciones
------------------------------------------------------------------------------*/
bool Get_Normal(double A1, double A2, double A3, double E1,
	double B1, double B2, double B3, double E2,
	double *nx1, double *ny1, double *nz1,
	double *nx2, double *ny2, double *nz2)
{
	double CCx1, CCy1, CCx2, CCy2;
	double AAA, BBB; 

	// Check parameters well defined 
	// (note "Pure" C reference parameter = pointer ==> *nx1, instead of & with C++ reference parameter ==> nx1)
	*nx1 = 0.0; *nx2 = 0.0; *ny1 = 0.0; *ny2 = 0.0; *nz1 = 0.0; *nz2 = 0.0;

	// Solution nombre 1: ----
	CCx1 = ((A3*B1 - A1 * B3)*(pow(A3, 2)*(pow(B1, 2) + pow(B2, 2)) - 2 * A1*A3*B1*B3 - 2 * A2*B2*(A1*B1 + A3 * B3) + pow(A2, 2)*(pow(B1, 2) + pow(B3, 2)) + pow(A1, 2)*(pow(B2, 2) + pow(B3, 2))));

	CCy1 = (pow(A3, 2)*(pow(B1, 2) + pow(B2, 2)) - 2 * A1*A3*B1*B3 - 2 * A2*B2*(A1*B1 + A3 * B3) + pow(A2, 2)*(pow(B1, 2) + pow(B3, 2)) + pow(A1, 2)*(pow(B2, 2) + pow(B3, 2)));

	if ((CCx1 == 0) || (CCy1 == 0))   // cannot find solution
		return(false);

	AAA = pow(A3*B1 - A1 * B3, 2)*(-(pow(B1, 2)*pow(E1, 2)) + (pow(B2, 2) + pow(B3, 2))*(A1 - E1)*(A1 + E1) + 2 * A1*B1*E1*E2 - pow(A1, 2)*pow(E2, 2) - 2 * A2*B2*(A1*B1 + A3 * B3 - E1 * E2) + A3 * (-2 * A1*B1*B3 + 2 * B3*E1*E2) + pow(A3, 2)*(pow(B1, 2) + pow(B2, 2) - pow(E2, 2)) + pow(A2, 2)*(pow(B1, 2) + pow(B3, 2) - pow(E2, 2)));
	if (AAA<0.0) AAA = 0.0; // avoiding round-off errors!! (strongly depending on compiler and its options!)

	BBB = pow(A3*B1 - A1 * B3, 2)*(-(pow(B1, 2)*pow(E1, 2)) + (pow(B2, 2) + pow(B3, 2))*(A1 - E1)*(A1 + E1) + 2 * A1*B1*E1*E2 - pow(A1, 2)*pow(E2, 2) - 2 * A2*B2*(A1*B1 + A3 * B3 - E1 * E2) + A3 * (-2 * A1*B1*B3 + 2 * B3*E1*E2) + pow(A3, 2)*(pow(B1, 2) + pow(B2, 2) - pow(E2, 2)) + pow(A2, 2)*(pow(B1, 2) + pow(B3, 2) - pow(E2, 2)));
	if (BBB<0.0) BBB = 0.0;

	*nx1 = (pow(A2, 2)*B1*(A3*B1 - A1 * B3)*E2 - A2 * B2*(A3*B1 - A1 * B3)*(B1*E1 + A1 * E2) + (A3*B1 - A1 * B3)*(A1*(pow(B2, 2) + pow(B3, 2))*E1 - A1 * A3*B3*E2 + A3 * B1*(-(B3*E1) + A3 * E2)) - A3 * B2*sqrt(AAA) + A2 * B3*sqrt(BBB)) / CCx1;

	AAA = pow(A3*B1 - A1 * B3, 2)* (-(pow(B1, 2)*pow(E1, 2)) + (pow(B2, 2) + pow(B3, 2))*(A1 - E1)*(A1 + E1) + 2 * A1*B1*E1*E2 - pow(A1, 2)*pow(E2, 2) - 2 * A2*B2*(A1*B1 + A3 * B3 - E1 * E2) + A3 * (-2 * A1*B1*B3 + 2 * B3*E1*E2) + pow(A3, 2)*(pow(B1, 2) + pow(B2, 2) - pow(E2, 2)) + pow(A2, 2)*(pow(B1, 2) + pow(B3, 2) - pow(E2, 2)));
	if (AAA<0.0) AAA = 0.0;

	*ny1 = (-(B2*(A1*B1 + A3 * B3)*E1) + A2 * (pow(B1, 2) + pow(B3, 2))*E1 + (pow(A1, 2) + pow(A3, 2))*B2*E2 - A2 * (A1*B1 + A3 * B3)*E2 + sqrt(AAA)) / CCy1;

	AAA = 1.0 - (*nx1) * (*nx1) - (*ny1) * (*ny1);
	if (AAA<0.0) AAA = 0.0;

	*nz1 = sqrt(AAA);

	// Solution number 2: ----
	CCx2 = ((A3*B1 - A1 * B3)*(pow(A3, 2)*(pow(B1, 2) + pow(B2, 2)) - 2 * A1*A3*B1*B3 - 2 * A2*B2*(A1*B1 + A3 * B3) + pow(A2, 2)*(pow(B1, 2) + pow(B3, 2)) + pow(A1, 2)*(pow(B2, 2) + pow(B3, 2))));

	CCy2 = (pow(A3, 2)*(pow(B1, 2) + pow(B2, 2)) - 2 * A1*A3*B1*B3 - 2 * A2*B2*(A1*B1 + A3 * B3) + pow(A2, 2)*(pow(B1, 2) + pow(B3, 2)) + pow(A1, 2)*(pow(B2, 2) + pow(B3, 2)));

	if ((CCx2 == 0) || (CCy2 == 0))   // Il n'y a pas de solution :-(
		return(false);

	AAA = pow(A3*B1 - A1 * B3, 2)*(-(pow(B1, 2)*pow(E1, 2)) + (pow(B2, 2) + pow(B3, 2))*(A1 - E1)*(A1 + E1) + 2 * A1*B1*E1*E2 - pow(A1, 2)*pow(E2, 2) - 2 * A2*B2*(A1*B1 + A3 * B3 - E1 * E2) + A3 * (-2 * A1*B1*B3 + 2 * B3*E1*E2) + pow(A3, 2)*(pow(B1, 2) + pow(B2, 2) - pow(E2, 2)) + pow(A2, 2)*(pow(B1, 2) + pow(B3, 2) - pow(E2, 2)));
	if (AAA<0.0) AAA = 0.0; // round-off errors!

	BBB = pow(A3*B1 - A1 * B3, 2)*(-(pow(B1, 2)*pow(E1, 2)) + (pow(B2, 2) + pow(B3, 2))*(A1 - E1)*(A1 + E1) + 2 * A1*B1*E1*E2 - pow(A1, 2)*pow(E2, 2) - 2 * A2*B2*(A1*B1 + A3 * B3 - E1 * E2) + A3 * (-2 * A1*B1*B3 + 2 * B3*E1*E2) + pow(A3, 2)*(pow(B1, 2) + pow(B2, 2) - pow(E2, 2)) + pow(A2, 2)*(pow(B1, 2) + pow(B3, 2) - pow(E2, 2)));
	if (BBB<0.0) BBB = 0.0; // round-off errors!

	*nx2 = (pow(A2, 2)*B1*(A3*B1 - A1 * B3)*E2 - A2 * B2*(A3*B1 - A1 * B3)*(B1*E1 + A1 * E2) + (A3*B1 - A1 * B3)*(A1*(pow(B2, 2) + pow(B3, 2))*E1 - A1 * A3*B3*E2 + A3 * B1*(-(B3*E1) + A3 * E2)) + A3 * B2*sqrt(AAA) - A2 * B3*sqrt(BBB)) / CCx2;

	AAA = pow(A3*B1 - A1 * B3, 2)* (-(pow(B1, 2)*pow(E1, 2)) + (pow(B2, 2) + pow(B3, 2))*(A1 - E1)*(A1 + E1) + 2 * A1*B1*E1*E2 - pow(A1, 2)*pow(E2, 2) - 2 * A2*B2*(A1*B1 + A3 * B3 - E1 * E2) + A3 * (-2 * A1*B1*B3 + 2 * B3*E1*E2) + pow(A3, 2)*(pow(B1, 2) + pow(B2, 2) - pow(E2, 2)) + pow(A2, 2)*(pow(B1, 2) + pow(B3, 2) - pow(E2, 2)));
	if (AAA<0.0) AAA = 0.0; // round-off errors!

	*ny2 = -((B2*(A1*B1 + A3 * B3)*E1 - A2 * (pow(B1, 2) + pow(B3, 2))*E1 - (pow(A1, 2) + pow(A3, 2))*B2*E2 + A2 * (A1*B1 + A3 * B3)*E2 + sqrt(AAA)) / CCy2);

	AAA = 1. - (*nx2) * (*nx2) - (*ny2) * (*ny2);
	if (AAA<0.0) AAA = 0.0; // round-off errors!

	*nz2 = sqrt(AAA);

	return(true);  // solution found!
}



// ============= LOCAL SOLVERS ======================
// 2D Case
void local_solver2D(double *phi, int *ordre, double *p, const int Np, int *tri, const int NTri,
	int *tet, const int NTetra, int numpointsDirichlet, node **inverse_map, int dim)
{
	int i, k;
	int pointA, pointB, pointC;
	double xA, yA, xB, yB, xC, yC;
	double zA=0.0, zB=0.0, zC=0.0;
	double phiA, phiB, phiC;
	double a, b, c;

	// LOOP in k: k=0 forth, k=1 back
	for (k = 0; k<2; k++) {
		// Read dirichlet nodes
		for (i = 1 + numpointsDirichlet; i<Np; i++) {
			if (k == 0)
				pointC = ordre[i]; // forth
			else
				pointC = ordre[Np + numpointsDirichlet - i]; // back

			phiC = phi[pointC];

			// ==> Loop in elements containing node C
			for (int ee = 0; ee<size_list(inverse_map[pointC]); ee++) {

				// Check only triangles (2D or 3D)
				if (NTetra==0) {
					int elem = get_value(inverse_map[pointC],ee);

					if (tri[0 + 3*elem] == pointC) {
						pointA = tri[1 + 3*elem];
						pointB = tri[2 + 3*elem];
					}
					else if (tri[1 + 3 * elem] == pointC) {
						pointA = tri[0 + 3 * elem];
						pointB = tri[2 + 3 * elem];
					}
					else if (tri[2 + 3 * elem] == pointC) {
						pointA = tri[0 + 3 * elem];
						pointB = tri[1 + 3 * elem];
					}

					xA = p[0 + dim * pointA]; // mesh.p[pointA].x;
					yA = p[1 + dim * pointA]; // mesh.p[pointA].y;
					xB = p[0 + dim * pointB];
					yB = p[1 + dim * pointB];
					xC = p[0 + dim * pointC];
					yC = p[1 + dim * pointC];
					
					if (dim == 3) {
						zA = p[2 + dim * pointA]; // mesh.p[pointA].z;
						zB = p[2 + dim * pointB];
						zC = p[2 + dim * pointC];
					}
					
					phiA = phi[pointA];
					phiB = phi[pointB];

					// Edge length in triangle
					c = norm_3_(xA - xB, yA - yB, zA - zB);
					b = norm_3_(xA - xC, yA - yC, zA - zC);
					a = norm_3_(xB - xC, yB - yC, zB - zC);

					// Set new value for phiC: Note f[C] = 1 in the Eikonal case!!
					if ((phiA >= 0 && phiB >= 0) && phiC>0)
						phiC = local_solver_triangleABC(a, b, c, phiA, phiB, phiC, 1.0); // MINIMUM
					else
						phiC = -local_solver_triangleABC(-a, -b, -c, -phiA, -phiB, -phiC, -1.0); // MAXIMUM	      
				} // only TRI
				else {
					mexErrMsgTxt("(local_solver2D): No triangles found!\n");
				}

				// Assign minimal value
				phi[pointC] = phiC;

			} // for ee ... inverse map
		} // for i ... numpoints dirichlet
	} // for (k ...) (forth & back)


#ifdef VERBOSE_
	for (int i = 0; i < Np; i++) {
		mexPrintf("phi[%d] = %f\n", i, phi[i]);
		mexEvalString("pause(.001);"); // to dump string.
	}
#endif

	return;
}



// 3D CASE
void local_solver3D(double *phi, int *ordre, double *p, const int Np, int *tri, const int NTri,
	int *tet, const int NTetra, int numpointsDirichlet, node **inverse_map, int dim)
{
	int i, k, pointA, pointB, pointC, pointD;
	double xA, yA, zA, xB, yB, zB, xC, yC, zC, xD, yD, zD;
	double phiA, phiB, phiC, phiD;
	double nx1, ny1, nz1, nx2, ny2, nz2;
	
	double ABx, ABy, ABz, ACx, ACy, ACz, ADx, ADy, ADz;
	double BCx, BCy, BCz, BDx, BDy, BDz;
	double CDx, CDy, CDz;
	double AB, AC, AD, BC, BD, CD;
	double G1, G2;

	bool intE1 = false, intE2 = false;
	
	// Sweep: k=0 forth, k=1 back
	for (k = 0; k<2; k++) {
#ifdef VERBOSE_
		if (k == 0)
			mexPrintf(">");
		else
			mexPrintf("<");
#endif // VERBOSE_

		for (i = 1 + numpointsDirichlet; i<Np; i++) {
			if (k == 0)
				pointD = ordre[i]; // forth
			else
				pointD = ordre[Np - 1 - i]; // back

			phiD = phi[pointD];

			for (int ee = 0; ee<size_list(inverse_map[pointD]); ee++) {

				int elem = get_value(inverse_map[pointD],ee);

				if (pointD == tet[0 + 4 * elem]) { // mesh.Tetra[elem].v[0]) {
					pointA = tet[1 + 4 * elem]; // mesh.Tetra[elem].v[1];
					pointB = tet[2 + 4 * elem]; // mesh.Tetra[elem].v[2];
					pointC = tet[3 + 4 * elem]; // mesh.Tetra[elem].v[3];
				}
				else if (pointD == tet[1 + 4 * elem]) {
					pointA = tet[0 + 4 * elem];
					pointB = tet[2 + 4 * elem];
					pointC = tet[3 + 4 * elem];
				}
				else if (pointD == tet[2 + 4 * elem]) {
					pointA = tet[0 + 4 * elem];
					pointB = tet[1 + 4 * elem];
					pointC = tet[3 + 4 * elem];
				}
				else {
					pointA = tet[0 + 4 * elem];
					pointB = tet[1 + 4 * elem];
					pointC = tet[2 + 4 * elem];
				}

				xA = p[0 + 3 * pointA]; yA = p[1 + 3 * pointA]; zA = p[2 + 3 * pointA];
				xB = p[0 + 3 * pointB]; yB = p[1 + 3 * pointB]; zB = p[2 + 3 * pointB];
				xC = p[0 + 3 * pointC]; yC = p[1 + 3 * pointC]; zC = p[2 + 3 * pointC];
				xD = p[0 + 3 * pointD]; yD = p[1 + 3 * pointD]; zD = p[2 + 3 * pointD]; 

				phiA = phi[pointA];
				phiB = phi[pointB];
				phiC = phi[pointC];

				ABx = xB - xA; ABy = yB - yA; ABz = zB - zA;
				ACx = xC - xA; ACy = yC - yA; ACz = zC - zA;
				ADx = xD - xA; ADy = yD - yA; ADz = zD - zA;
				BDx = xD - xB; BDy = yD - yB; BDz = zD - zB;
				BCx = xC - xB; BCy = yC - yB; BCz = zC - zB;
				CDx = xD - xC; CDy = yD - yC; CDz = zD - zC;
				AB = sqrt(ABx*ABx + ABy * ABy + ABz * ABz);
				AC = sqrt(ACx*ACx + ACy * ACy + ACz * ACz);
				AD = sqrt(ADx*ADx + ADy * ADy + ADz * ADz);
				BD = sqrt(BDx*BDx + BDy * BDy + BDz * BDz);
				BC = sqrt(BCx*BCx + BCy * BCy + BCz * BCz);
				CD = sqrt(CDx*CDx + CDy * CDy + CDz * CDz);

				// ---- case phi>0 -----------------------------------------------------
				if ((phiA >= 0) && (phiB >= 0) && (phiC >= 0) && (phiD>0)) {

					// min(phiA,..,phiC) for solver
					if ((phiA <= phiB) && (phiA <= phiC)) {
						G1 = fabs(phiB - phiA);
						G2 = fabs(phiC - phiA);

						//  AB.n = G1/fD  y AC.n = G2/fD : Causality, but fD=1 now
						if ((G1 <= AB) && (G2 <= AC)) {

							// projection of D on plane ABC (point E)	  
							//bool normal_inside = Get_Normal(ABx, ABy, ABz, G1 / fD, ACx, ACy, ACz, G2 / fD, nx1, ny1, nz1, nx2, ny2, nz2);
							// Here: Pure C reference parameter:
							bool normal_inside = Get_Normal(ABx, ABy, ABz, G1, ACx, ACy, ACz, G2, &nx1, &ny1, &nz1, &nx2, &ny2, &nz2);

							if (!normal_inside) {
								// Does E1 fall inside triangle ABC ?
								intE1 = projectionABC(xA, yA, zA, xB, yB, zB, xC, yC, zC, xD, yD, zD, nx1, ny1, nz1);
								// Does E2 fall inside triangle ABC ?
								intE2 = projectionABC(xA, yA, zA, xB, yB, zB, xC, yC, zC, xD, yD, zD, nx2, ny2, nz2);

								// If neither E1 nor E2 falls inside the element
								if ((!intE1) && (!intE2))
									normal_inside = false;
								else if (!intE2)
									phiD = MIN(phiD, phiA + fabs(ADx*nx1 + ADy * ny1 + ADz * nz1)); // ... nz2)*fD
								else if (!intE1)
									phiD = MIN(phiD, phiA + fabs(ADx*nx2 + ADy * ny2 + ADz * nz2)); // ... nz2)*fD
							} // end normal_inside

							if (normal_inside) {
								// Apply local_solver_...ABC(a,b,c,phiA,phiB,phiC,fc) to get minimum
								// triangle ABD
								phiD = local_solver_triangleABC(BD, AD, AB, phiA, phiB, phiD, 1.0); // fD=1
								// triangle ACD
								phiD = local_solver_triangleABC(CD, AD, AC, phiA, phiC, phiD, 1.0);
								// triangle BCD
								phiD = local_solver_triangleABC(CD, BD, BC, phiB, phiC, phiD, 1.0);
							}

						} else { // Causality violation: Get minimum
							phiD = local_solver_triangleABC(BD, AD, AB, phiA, phiB, phiD, 1.0);
							phiD = local_solver_triangleABC(CD, AD, AC, phiA, phiC, phiD, 1.0);
							phiD = local_solver_triangleABC(CD, BD, BC, phiB, phiC, phiD, 1.0);
						}
					} // End MIN = phiA

					if ((phiA >= phiB) && (phiB <= phiC)) {// phiB is minimum
						G1 = fabs(phiA - phiB);
						G2 = fabs(phiC - phiB);

						//  BA*n = G1/fD  et BC*n = G2/fD : Causality (fD=1)
						if ((G1 <= AB) && (G2 <= BC)) {

							bool normal_inside = Get_Normal(-ABx, -ABy, -ABz, G1 , BCx, BCy, BCz, G2 , &nx1, &ny1, &nz1, &nx2, &ny2, &nz2);

							if (!normal_inside) {
								intE1 = projectionABC(xA, yA, zA, xB, yB, zB, xC, yC, zC, xD, yD, zD, nx1, ny1, nz1);
								intE2 = projectionABC(xA, yA, zA, xB, yB, zB, xC, yC, zC, xD, yD, zD, nx2, ny2, nz2);

								if ((! intE1) && (! intE2)) 
									normal_inside = false;
								else if (! intE2)
									phiD = MIN(phiD, phiB + fabs(BDx*nx1 + BDy * ny1 + BDz * nz1)); 
								else if (! intE1)
									phiD = MIN(phiD, phiB + fabs(BDx*nx2 + BDy * ny2 + BDz * nz2)); 
							} // fin normal_inside

							

							if (normal_inside) {
								phiD = local_solver_triangleABC(BD, AD, AB, phiA, phiB, phiD, 1.0);
								phiD = local_solver_triangleABC(CD, AD, AC, phiA, phiC, phiD, 1.0);
								phiD = local_solver_triangleABC(CD, BD, BC, phiB, phiC, phiD, 1.0);
							}
						} else { // causality violation
							phiD = local_solver_triangleABC(BD, AD, AB, phiA, phiB, phiD, 1.0);
							phiD = local_solver_triangleABC(CD, AD, AC, phiA, phiC, phiD, 1.0);
							phiD = local_solver_triangleABC(CD, BD, BC, phiB, phiC, phiD, 1.0);
						}
					} // end MIN = phiB

					if ((phiA >= phiC) && (phiB >= phiC)) {// MIN = phiC
						G1 = fabs(phiA - phiC);
						G2 = fabs(phiB - phiC);

						if ((G1 <= AC) && (G2 <= BC)) {// Causality
							bool normal_inside = Get_Normal(-ACx, -ACy, -ACz, G1, -BCx, -BCy, -BCz, G2, &nx1, &ny1, &nz1, &nx2, &ny2, &nz2);

							if (! normal_inside) {
								intE1 = projectionABC(xA, yA, zA, xB, yB, zB, xC, yC, zC, xD, yD, zD, nx1, ny1, nz1);
								intE2 = projectionABC(xA, yA, zA, xB, yB, zB, xC, yC, zC, xD, yD, zD, nx2, ny2, nz2);

								if ((! intE1) && (! intE2))
									normal_inside = false;
								else if (! intE2)
									phiD = MIN(phiD, phiC + fabs(CDx*nx1 + CDy * ny1 + CDz * nz1));
								else if (! intE1)
									phiD = MIN(phiD, phiC + fabs(CDx*nx2 + CDy * ny2 + CDz * nz2));
							} // end normal_inside

							if (normal_inside) {
								phiD = local_solver_triangleABC(BD, AD, AB, phiA, phiB, phiD, 1.0);
								phiD = local_solver_triangleABC(CD, AD, AC, phiA, phiC, phiD, 1.0);
								phiD = local_solver_triangleABC(CD, BD, BC, phiB, phiC, phiD, 1.0);
							}

						} else { // causality violation
							phiD = local_solver_triangleABC(BD, AD, AB, phiA, phiB, phiD, 1.0);
							phiD = local_solver_triangleABC(CD, AD, AC, phiA, phiC, phiD, 1.0);
							phiD = local_solver_triangleABC(CD, BD, BC, phiB, phiC, phiD, 1.0);
						}
					} // end MIN = phiC
				} else { // if PHI<0:

					// Search MAXIMUM in (phiA,..,phiC) for the solver
					if ((phiA >= phiB) && (phiA >= phiC)) {// ****** If phiA is MAX
						G1 = fabs(phiB - phiA);
						G2 = fabs(phiC - phiA);

						if ((G1 <= AB) && (G2 <= AC)) {

							bool normal_inside = Get_Normal(ABx, ABy, ABz, G1, ACx, ACy, ACz, G2, &nx1, &ny1, &nz1, &nx2, &ny2, &nz2);

							if (! normal_inside) {
								intE1 = projectionABC(xA, yA, zA, xB, yB, zB, xC, yC, zC, xD, yD, zD, nx1, ny1, nz1);
								intE2 = projectionABC(xA, yA, zA, xB, yB, zB, xC, yC, zC, xD, yD, zD, nx2, ny2, nz2);

								if ((! intE1) && (! intE2))
									normal_inside = false;
								else if (! intE2)
									phiD = MAX(phiD, phiA - fabs(ADx*nx1 + ADy * ny1 + ADz * nz1)); 
								else if (! intE1)
									phiD = MAX(phiD, phiA - fabs(ADx*nx2 + ADy * ny2 + ADz * nz2));
							} // end normal_inside

							if (normal_inside) {
								phiD = -local_solver_triangleABC(-BD, -AD, -AB, -phiA, -phiB, -phiD, -1.0);
								phiD = -local_solver_triangleABC(-CD, -AD, -AC, -phiA, -phiC, -phiD, -1.0);
								phiD = -local_solver_triangleABC(-CD, -BD, -BC, -phiB, -phiC, -phiD, -1.0);
							}

						} else { // causality violation
							phiD = -local_solver_triangleABC(-BD, -AD, -AB, -phiA, -phiB, -phiD, -1.0);
							phiD = -local_solver_triangleABC(-CD, -AD, -AC, -phiA, -phiC, -phiD, -1.0);
							phiD = -local_solver_triangleABC(-CD, -BD, -BC, -phiB, -phiC, -phiD, -1.0);
						}

					} // end MAX = phiA

					if ((phiA <= phiB) && (phiB >= phiC)) {// ******** If phiB is MAX
						G1 = fabs(phiA - phiB);
						G2 = fabs(phiC - phiB);

						if ((G1 <= AB) && (G2 <= BC)) {
							bool normal_inside = Get_Normal(-ABx, -ABy, -ABz, G1, BCx, BCy, BCz, G2, &nx1, &ny1, &nz1, &nx2, &ny2, &nz2);

							if (! normal_inside) {
								intE1 = projectionABC(xA, yA, zA, xB, yB, zB, xC, yC, zC, xD, yD, zD, nx1, ny1, nz1);
								intE2 = projectionABC(xA, yA, zA, xB, yB, zB, xC, yC, zC, xD, yD, zD, nx2, ny2, nz2);

								if ((! intE1) && (! intE2)) 
									normal_inside = false;
								else if (! intE2)
									phiD = MAX(phiD, phiB - fabs(BDx*nx1 + BDy * ny1 + BDz * nz1));
								else if (! intE1)
									phiD = MAX(phiD, phiB - fabs(BDx*nx2 + BDy * ny2 + BDz * nz2));
							} // end normal_inside

							if (normal_inside) {
								phiD = -local_solver_triangleABC(-BD, -AD, -AB, -phiA, -phiB, -phiD, -1.0);
								phiD = -local_solver_triangleABC(-CD, -AD, -AC, -phiA, -phiC, -phiD, -1.0);
								phiD = -local_solver_triangleABC(-CD, -BD, -BC, -phiB, -phiC, -phiD, -1.0);
							}
						} else { // causality violation
							phiD = -local_solver_triangleABC(-BD, -AD, -AB, -phiA, -phiB, -phiD, -1.0);
							phiD = -local_solver_triangleABC(-CD, -AD, -AC, -phiA, -phiC, -phiD, -1.0);
							phiD = -local_solver_triangleABC(-CD, -BD, -BC, -phiB, -phiC, -phiD, -1.0);
						}
					} // end MAX = phiB

					if ((phiA <= phiC) && (phiB <= phiC)) {// ******** If phiC is MAX
						G1 = fabs(phiA - phiC);
						G2 = fabs(phiB - phiC);

						if ((G1 <= AC) && (G2 <= BC)) {
							bool normal_inside = Get_Normal(-ACx, -ACy, -ACz, G1, -BCx, -BCy, -BCz, G2, &nx1, &ny1, &nz1, &nx2, &ny2, &nz2);

							if (! normal_inside) {
								intE1 = projectionABC(xA, yA, zA, xB, yB, zB, xC, yC, zC, xD, yD, zD, nx1, ny1, nz1);
								intE2 = projectionABC(xA, yA, zA, xB, yB, zB, xC, yC, zC, xD, yD, zD, nx2, ny2, nz2);

								if ((! intE1) && (! intE2))
									normal_inside = false;
								else if (! intE2)
									phiD = MAX(phiD, phiC - fabs(CDx*nx1 + CDy * ny1 + CDz * nz1));
								else if (! intE1)
									phiD = MAX(phiD, phiC - fabs(CDx*nx2 + CDy * ny2 + CDz * nz2));
							} // end normal_inside

							if (normal_inside) {
								phiD = -local_solver_triangleABC(-BD, -AD, -AB, -phiA, -phiB, -phiD, -1.0);
								phiD = -local_solver_triangleABC(-CD, -AD, -AC, -phiA, -phiC, -phiD, -1.0);
								phiD = -local_solver_triangleABC(-CD, -BD, -BC, -phiB, -phiC, -phiD, -1.0);
							}

						} else { // causality violation
							phiD = -local_solver_triangleABC(-BD, -AD, -AB, -phiA, -phiB, -phiD, -1.0);
							phiD = -local_solver_triangleABC(-CD, -AD, -AC, -phiA, -phiC, -phiD, -1.0);
							phiD = -local_solver_triangleABC(-CD, -BD, -BC, -phiB, -phiC, -phiD, -1.0);
						}
					} // end MIN = phiC

				} // end else if PHI<0 -----------------------

			} // end elements (ee) <--- D

			  // At the end, get the minumum
			phi[pointD] = phiD;

		} // for (i ...) points Dirichlet


	} //  for (k ...) [forth & back]

	return;
}
