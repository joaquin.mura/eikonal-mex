% fastsweeping.m Help file for MEX-file.
%  fastsweeping.c - Solves the Eikonal problem
%   
%    abs(grad(u)) = 1  in the mesh
%          u(ref) = 0  on reference nodes
%
% 	Based on "Fast Sweeping Methods for Eikonal Equations on Triangular Meshes"
%   Jianliang Qian, Yong-Tao Zhang, and Hong-Kai Zhao
%   SIAM J. Numer. Anal., 45(1), 83-107.
%   https://doi.org/10.1137/050627083
%
% Usage (2D):
%  u = fastsweeping(p,tri,[],ref);
%
% Usage (3D):
%  u = fastsweeping(p,[],tetra,ref);
%
% where
%  p     : coordinates. Size = dim x #nodes, dim=2 or 3.
%  tri   : mesh connectivity (triangles). Size = 3x #triangles
%  tetra : mesh connectivity (tetrahedra). Size = 4x #tetrahedra
%  ref   : List with reference nodes.
%  u     : nodal solution
%  
% Created by Joaquin Mura (October 28, 2019).
%
