% Test Fast-Sweeping
clear;
close all;

%% 2D
disp(' -- test 2d --');
load('mesh2d.mat');

pdeplot(p,tri); % display mesh

sol2d = fastsweeping(p,tri,[],ref);

figure(1);
trisurf(tri',p(1,:),p(2,:),sol2d,'facecolor','interp');
hold on;
plot3(p(1,ref),p(2,ref),0*p(1,ref),'ro','MarkerFaceColor',[0.5,0.5,0.5]);
hold off;

%% 3D
disp(' -- test 3d --');
load('mesh3d.mat');

% REMARK: array 'tri' is only used for visualization 
% in figure(2). Otherwise, it will compute the distance along
% the surface only.
sol3d = fastsweeping(p,[],tetra,ref);

figure(2);
trisurf(tri',p(1,:),p(2,:),p(3,:),sol3d,'facecolor','interp');
hold on;
plot3(p(1,ref),p(2,ref),p(3,ref),'ro','MarkerFaceColor',[0.5,0.5,0.5]);
hold off;
axis equal;